package game.editor.modify;

public class ModifyRemoveEvent extends ModifySetEvent<ModifyRemoveEventData> {

    public ModifyRemoveEvent(Object source, ModifyRemoveEventData data) {
        super(source, data);
    }

}

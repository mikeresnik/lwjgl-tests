package game.logic;

public abstract class LogicObject {

    public abstract void process();

}

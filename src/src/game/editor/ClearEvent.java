package game.editor;

public class ClearEvent extends EditorEvent<ClearEventData> {

    public ClearEvent(Object source, ClearEventData data) {
        super(source, data);
    }

}
